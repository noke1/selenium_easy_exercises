package input_forms;

import core.CoreTest;
import helper.pages.input_forms.JQuerySelect;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class JQuerySelectTest extends CoreTest {

    private JQuerySelect jQuerySelect;

    @Test
    public void checkIfChosenOptionIsVisibleInSelect() {
        jQuerySelect = new JQuerySelect(getDriver());
        visit();
        final String country = "Bangladesh";

        jQuerySelect.chooseCountry(country);
        final String selectCountryText = jQuerySelect.getSelectCountryText();

        assertThat(selectCountryText).isEqualTo(country);
    }

    @Test
    public void checkIfSearchedAndChosenOptionIsVisibleInSelect() {
        jQuerySelect = new JQuerySelect(getDriver());
        visit();
        final String country = "Japan";

        jQuerySelect
                .searchForCountry(country)
                .chooseCountry(country);
        final String selectCountryText = jQuerySelect.getSelectCountryText();

        assertThat(selectCountryText).isEqualTo(country);
    }
    //-------------------------------------
    @Test(groups = {"smoke"})
    public void checkIfSearchedAndClickedValueIsVisibleInMultiSelect() {
        jQuerySelect = new JQuerySelect(getDriver());
        visit();
        final List<String> states = Arrays.asList("Alaska","Alabama");

        jQuerySelect.searchForState(states);
        List<String> chosenStates = jQuerySelect.getChosenStates();

        assertThat(chosenStates).containsAll(states);
    }

    @Test
    public void checkIfChosenStatesAreRemoved() {
        jQuerySelect = new JQuerySelect(getDriver());
        visit();
        final List<String> states = Arrays.asList("New Mexico","Colorado");

        jQuerySelect
                .searchForState(states)
                .removeChosenStates();
        List<String> chosenStates = jQuerySelect.getChosenStates();

        assertThat(chosenStates).isEmpty();
    }

    private void visit() {
        getBrowserTools().visit("https://www.seleniumeasy.com/test/jquery-dropdown-search-demo.html");
    }

}

package progress_bar_sliders;

import core.CoreTest;
import helper.pages.progress_bar_sliders.BootstrapProgressBar;
import org.assertj.core.api.Assertions;
import org.testng.annotations.Test;

public class BootstrapProgressBarTest extends CoreTest {


    private BootstrapProgressBar bootstrapProgressBar;

    @Test
    public void shouldCheckIfCompleteAlertIsDisplayedAfterSuccessfulDownload() {
        bootstrapProgressBar = new BootstrapProgressBar(getDriver());
        visit();

        bootstrapProgressBar.startDownload();
        boolean isCompleteDisplayed = bootstrapProgressBar.waitForDownloadToFinish();

        Assertions.assertThat(isCompleteDisplayed).isTrue();
    }

    private void visit() {
        getBrowserTools().visit("https://www.seleniumeasy.com/test/bootstrap-download-progress-demo.html");
    }


}
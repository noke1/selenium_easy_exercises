package helper.pages.input_forms;

import helper.others.waiter.Waiter;
import helper.pages.core.CorePage;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.util.List;
import java.util.stream.Collectors;

import static helper.others.waiter.Waiter.waitForElementToAppear;

public class JQuerySelect extends CorePage {

    public JQuerySelect(WebDriver driver) {
        super(driver);
    }

    //Drop Down with Search box------------------------------
    @FindBy(css = "select#country")
    private WebElement selectCountry;

    @FindBy(css = "#country option")
    private List<WebElement> countryOptions;

    @FindBy(css = "span > input.select2-search__field")
    private WebElement searchBoxSelectCountry;

    @FindBy(css = "#select2-country-container + span.select2-selection__arrow")
    private WebElement selectCountryArrowDown;

    //Select Multiple Values---------------------------------
    @FindBy(css = ".select2-search__field")
    private WebElement multiSelectField;

    @FindBy(css = ".select2-selection__choice")
    private List<WebElement> multiSelectChosenStates;

    @FindBy(css = "span.select2-selection__choice__remove")
    private List<WebElement> multiSelectRemoveSign;

    public JQuerySelect chooseCountry(String country) {
        waitForElementToAppear(selectCountry);
        Select select = new Select(selectCountry);
        select.selectByValue(country);
        return this;
    }

    public JQuerySelect searchForCountry(String country) {
        waitForElementToAppear(selectCountryArrowDown);
        selectCountryArrowDown.click();
        waitForElementToAppear(searchBoxSelectCountry);
        searchBoxSelectCountry.sendKeys(country);
        return this;
    }

    public String getSelectCountryText() {
        waitForElementToAppear(selectCountry);
        Select select = new Select(selectCountry);
        return select.getFirstSelectedOption().getText();
    }

    //-------------------------------------------------------------
    public JQuerySelect searchForState(List<String> states) {
        waitForElementToAppear(multiSelectField);
        for (String state : states) {
            multiSelectField.sendKeys(state);
            multiSelectField.sendKeys(Keys.ENTER);
        }
        return this;
    }

    public List<String> getChosenStates() {
        multiSelectChosenStates.forEach(Waiter::waitForElementToAppear);
        return multiSelectChosenStates
                .stream()
                .map(WebElement::getText)
                .map(this::deleteRemoveSignFromStateName)
                .collect(Collectors.toList());
    }

    public JQuerySelect removeChosenStates() {
        //I need to loop from the back when we talk about removing stuff from List.
        //If i loop forward StaleElementReferenceException appears.
        for (int i = multiSelectRemoveSign.size() - 1; i >= 0; i--) {
            WebElement element = multiSelectRemoveSign.get(i);
            waitForElementToAppear(element);
            element.click();
        }
        return this;
    }

    private String deleteRemoveSignFromStateName(String stateName) {
        return stateName.replaceAll("×", "");
    }


}

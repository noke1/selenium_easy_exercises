package input_forms;

import core.CoreTest;
import helper.pages.input_forms.SelectDropdownDemo;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class SelectDropdownDemoTest extends CoreTest {

    private SelectDropdownDemo selectDropdownDemo;

    @Test(dataProvider = "daysOfWeek")
    public void checkIfCorrectSummaryAppearsAfterChoosingEachDay(Object dayOfWeek) {
        selectDropdownDemo = new SelectDropdownDemo(getDriver());
        visit();

        selectDropdownDemo.chooseDay(String.valueOf(dayOfWeek));
        final String summary = selectDropdownDemo.getSummaryAfterDayChosen();

        assertThat(summary).isEqualTo(String.format(("Day selected :- %s"), dayOfWeek));
    }

    @DataProvider(name = "daysOfWeek")
    private Object[][] createData() {
        return new String[][]{
                {"Monday"},
                {"Tuesday"},
                {"Sunday"},
                {"Wednesday"},
                {"Thursday"},
                {"Friday"},
                {"Saturday"},
        };
    }

    @Test
    public void checkIfOneResultAppearsAfterSelectingOneStateAndClickingFirstSelected() {
        selectDropdownDemo = new SelectDropdownDemo(getDriver());
        visit();

        selectDropdownDemo
                .chooseState("California")
                .clickButtonFirstSelected();
        final String summaryForSelectedStates = selectDropdownDemo.getSummaryForSelectedStates();

        assertThat(summaryForSelectedStates).isEqualTo(String.format(("First selected option is : %s"),"California"));
    }

    @Test
    public void checkIfOneResultAppearsAfterSelectingOneStateAndClickingGetAllSelected() {
        selectDropdownDemo = new SelectDropdownDemo(getDriver());
        visit();

        selectDropdownDemo
                .chooseState("California")
                .clickButtonGetAllSelected();
        final String summaryForSelectedStates = selectDropdownDemo.getSummaryForSelectedStates();

        assertThat(summaryForSelectedStates).isEqualTo(String.format(("Options selected are : %s"),"California"));
    }

    @Test
    public void checkIfOneResultAppearsAfterSelectingAllStatesAndClickingFirstSelected() {
        selectDropdownDemo = new SelectDropdownDemo(getDriver());
        visit();

        selectDropdownDemo
                .chooseState("California")
                .chooseState("Florida")
                .chooseState("New Jersey")
                .chooseState("New York")
                .chooseState("Ohio")
                .chooseState("Texas")
                .chooseState("Pennsylvania")
                .chooseState("Washington")
                .clickButtonFirstSelected();
        final String summaryForSelectedStates = selectDropdownDemo.getSummaryForSelectedStates();

        assertThat(summaryForSelectedStates).isEqualTo(String.format(("First selected option is : %s"),"California"));
    }

    @Test(groups = {"smoke"})
    public void checkIfAllResultsAppearsAfterSelectingAllStatesAndClickingGetAllSelected() {
        selectDropdownDemo = new SelectDropdownDemo(getDriver());
        visit();

        selectDropdownDemo
                .chooseState("California")
                .chooseState("Florida")
                .chooseState("New Jersey")
                .chooseState("New York")
                .chooseState("Ohio")
                .chooseState("Texas")
                .chooseState("Pennsylvania")
                .chooseState("Washington")
                .clickButtonGetAllSelected();
        final String summaryForSelectedStates = selectDropdownDemo.getSummaryForSelectedStates();

        assertThat(summaryForSelectedStates).isEqualTo(
                String.format(("Options selected are : %s,%s,%s,%s,%s,%s,%s,%s"),
                        "California","Florida","New Jersey","New York","Ohio","Texas","Pennsylvania","Washington")
        );
    }



    private void visit() {
        getBrowserTools().visit("https://www.seleniumeasy.com/test/basic-select-dropdown-demo.html");
    }
}

package helper.browser;

import io.github.bonigarcia.wdm.WebDriverManager;
import io.github.bonigarcia.wdm.config.DriverManagerType;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class BrowserFactory {

    public WebDriver createInstance(String browserName) {
        WebDriver driverInstance;
        MutableCapabilities options = new OptionsFactory().getOptionsFor(browserName);
        DriverManagerType driverManagerType = DriverManagerType.valueOf(browserName.toUpperCase());

        switch (driverManagerType) {
            case CHROME:
                WebDriverManager.chromedriver().driverVersion("86.0.4240.22").setup();
                driverInstance = new ChromeDriver(options);
                break;
            case FIREFOX:
                WebDriverManager.firefoxdriver().browserVersion("88.0.4324.96").setup();
                driverInstance = new FirefoxDriver(options);
                break;
            default:
                throw new IllegalArgumentException("Wrong browser name");
        }
        return driverInstance;
    }





}

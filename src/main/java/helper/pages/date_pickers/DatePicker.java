package helper.pages.date_pickers;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.time.LocalDate;
import java.time.Month;
import java.time.YearMonth;
import java.util.List;

public class DatePicker  {

    private WebElement datePicker;
    private WebElement actualDateAndYear;
    private WebElement arrowPreviousMonth;
    private WebElement arrowNextMonth;
    private List<WebElement> daysAvailable;
    private List<WebElement> daysDisabled;

    public DatePicker(WebElement datePicker) {
        this.datePicker = datePicker;
        this.actualDateAndYear = datePicker.findElement(By.cssSelector(".datepicker-days .datepicker-switch"));
        this.arrowPreviousMonth = datePicker.findElement(By.cssSelector(".datepicker-days .prev"));
        this.arrowNextMonth = datePicker.findElement(By.cssSelector(".datepicker-days .next"));
        this.daysAvailable = datePicker.findElements(By.cssSelector(".datepicker-days td[class='day']"));
        this.daysDisabled = datePicker.findElements(By.cssSelector("td.disabled.disabled-date.day"));
    }

    public void selectDateOf(LocalDate chosenDate) {
        int chosenDay = chosenDate.getDayOfMonth();
        Month chosenMonth = chosenDate.getMonth();
        int chosenYear = chosenDate.getYear();
        String monthAndYear = chosenMonth + " " + chosenYear;

        while (!(actualDateAndYear.getText().equalsIgnoreCase(monthAndYear))) {
            final String[] x = actualDateAndYear.getText().split(" ");
            final int actualYear = Integer.parseInt(x[1]);
            final int actualMonth = Month.valueOf(x[0].toUpperCase()).getValue();

            final YearMonth actual = YearMonth.of(actualYear, actualMonth);
            final YearMonth chosen = YearMonth.of(chosenYear, chosenMonth);

            if(actual.compareTo(chosen) > 0) goToPreviousMonth();
            else if (actual.compareTo(chosen) == 0) break;
            else goToNextMonth();
        }

        chooseDayOf(chosenDay);
    }

    private void chooseDayOf(int chosenDay) {
        if(isDisabled(chosenDay)) throw new IllegalArgumentException(String.format("Chosen day %d is disabled! Please choose day which is available.", chosenDay));

        this.daysAvailable = datePicker.findElements(By.cssSelector(".datepicker-days td[class='day']"));
        for (WebElement day : daysAvailable) {
            final int dayNumber = Integer.parseInt(day.getText());
            if (dayNumber == chosenDay) {
                day.click();
                break;
            }
        }
    }

    private boolean isDisabled(int chosenDay) {
        this.daysDisabled = datePicker.findElements(By.cssSelector("td.disabled.disabled-date.day"));
        return daysDisabled.stream()
                .mapToInt(day -> Integer.parseInt(day.getText()))
                .anyMatch(number -> number == chosenDay);
    }

    private void goToPreviousMonth() {
        arrowPreviousMonth.click();
    }

    private void goToNextMonth() {
        arrowNextMonth.click();
    }
}

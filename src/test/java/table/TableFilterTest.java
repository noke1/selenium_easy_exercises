package table;

import core.CoreTest;
import helper.others.pojo.Color;
import helper.pages.table.TableFilter;
import org.testng.annotations.Test;

import java.util.List;

import static helper.others.pojo.ColorOption.*;
import static org.assertj.core.api.Assertions.assertThat;

public class TableFilterTest extends CoreTest {

    private TableFilter tableFilter;

    @Test
    public void shouldFilterRecordsByGreen(){
        tableFilter = new TableFilter(getDriver());
        visit();

        tableFilter.filterBy(GREEN);
        List<Color> filterResults = tableFilter.getFilterResults();

        assertThat(filterResults)
                .map(Color::getColorName)
                .allMatch(x -> x.equalsIgnoreCase(GREEN.name()));
    }

    @Test
    public void shouldFilterRecordsByRed(){
        tableFilter = new TableFilter(getDriver());
        visit();

        tableFilter.filterBy(RED);
        List<Color> filterResults = tableFilter.getFilterResults();

        assertThat(filterResults)
                .map(Color::getColorName)
                .allMatch(x -> x.equalsIgnoreCase(RED.name()));
    }

    @Test
    public void shouldFilterRecordsByOrange(){
        tableFilter = new TableFilter(getDriver());
        visit();

        tableFilter.filterBy(ORANGE);
        List<Color> filterResults = tableFilter.getFilterResults();

        assertThat(filterResults)
                .map(Color::getColorName)
                .allMatch(x -> x.equalsIgnoreCase(ORANGE.name()));
    }

    @Test(groups = {"smoke"})
    public void shouldFilterRecordsByAll(){
        tableFilter = new TableFilter(getDriver());
        visit();

        tableFilter.filterBy(ALL);
        List<Color> filterResults = tableFilter.getFilterResults();

        assertThat(filterResults)
                .map(Color::getColorName)
                .allMatch(x -> x.equalsIgnoreCase(ORANGE.name()) ||
                        x.equalsIgnoreCase(GREEN.name()) ||
                        x.equalsIgnoreCase(RED.name())
                );
    }


    private void visit(){
        getBrowserTools().visit("https://www.seleniumeasy.com/test/table-records-filter-demo.html");
    }

}
package table;

import core.CoreTest;
import helper.others.pojo.Pagination;
import helper.pages.table.TableSortAndSearch;
import org.openqa.selenium.WebElement;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class TableSortAndSearchTest extends CoreTest {

    private TableSortAndSearch tableSortAndSearch;

    @Test(dataProvider = "getPaginationOptions")
    public void shouldCheckIf(Pagination option) {
        tableSortAndSearch = new TableSortAndSearch(getDriver());
        visit();

        tableSortAndSearch.paginateTable(option);
        List<WebElement> searchResults = tableSortAndSearch.getTableRecords();

        assertThat(searchResults.size()).isBetween(0, option.getNumber());
    }

    @DataProvider
    public Object[][] getPaginationOptions() {
        final Pagination[] values = Pagination.values();
        final Object[][] objects = new Object[4][1];
        for (int i = 0; i < objects.length; i++) {
            objects[i][0] = values[i];
        }
        return objects;
    }



    public void visit(){
        getBrowserTools().visit("https://www.seleniumeasy.com/test/table-sort-search-demo.html");
    }
}
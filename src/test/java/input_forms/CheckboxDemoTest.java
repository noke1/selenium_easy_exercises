package input_forms;

import core.CoreTest;
import helper.pages.input_forms.CheckboxDemo;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class CheckboxDemoTest extends CoreTest {

    private CheckboxDemo checkboxDemo;

    @Test
    public void checkIfSuccessMessageAppearsAfterCheckboxSelect() {
        checkboxDemo = new CheckboxDemo(getDriver());
        visit();

        checkboxDemo.selectCheckboxAge();
        final String successMessage = checkboxDemo.getSuccessMessage();

        assertThat(successMessage).isEqualTo("Success - Check box is checked");
    }

    @Test
    public void checkIfAll4CheckboxesGetSelectedAfterButtonClick() {
        checkboxDemo = new CheckboxDemo(getDriver());
        visit();

        checkboxDemo.clickCheckAll();
        final boolean isCheckboxSelected = checkboxDemo.isCheckboxesSelected();

        assertThat(isCheckboxSelected).isTrue();
    }

    @Test(groups = {"smoke"})
    public void checkIfButtonNameChangesAfterSelectingAll4Checkboxes() {
        checkboxDemo = new CheckboxDemo(getDriver());
        visit();

        checkboxDemo.clickCheckAll();
        final String buttonText = checkboxDemo.getCheckAllButtonText();

        assertThat(buttonText).isEqualTo("Uncheck All");
    }

    @Test
    public void checkIfButtonNameRevertsAfterDeselecting1RandomCheckbox() {
        checkboxDemo = new CheckboxDemo(getDriver());
        visit();

        checkboxDemo
                .clickCheckAll()
                .deselectOneRandomCheckbox();
        final String buttonText = checkboxDemo.getCheckAllButtonText();

        assertThat(buttonText).isEqualTo("Check All");
    }

    private void visit() {
        getBrowserTools().visit("https://www.seleniumeasy.com/test/basic-checkbox-demo.html");
    }

}

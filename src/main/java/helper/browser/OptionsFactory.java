package helper.browser;

import io.github.bonigarcia.wdm.config.DriverManagerType;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;

public class OptionsFactory {

    public MutableCapabilities getOptionsFor(String browserName) {
        DriverManagerType driverManagerType = DriverManagerType.valueOf(browserName.toUpperCase());

        switch (driverManagerType) {
            case CHROME:
                return getChromeOptions();
            case FIREFOX:
                return getFirefoxOptions();
            default:
                throw new IllegalArgumentException("Wrong browser name");
        }
    }

    private FirefoxOptions getFirefoxOptions() {
        FirefoxOptions firefoxOptions = new FirefoxOptions();
        firefoxOptions.setUnhandledPromptBehaviour(UnexpectedAlertBehaviour.ACCEPT);
        return firefoxOptions;
    }

    private ChromeOptions getChromeOptions() {
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setUnhandledPromptBehaviour(UnexpectedAlertBehaviour.ACCEPT);
        chromeOptions.addArguments("disable-infobars");
        return chromeOptions;
    }
}

package table;

import core.CoreTest;
import helper.pages.table.TablePagination;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.*;

public class TablePaginationTest extends CoreTest {

    private TablePagination tablePagination;

    @Test(groups = {"smoke"})
    public void shouldCheckIfTableHasLessThan15Rows(){
        tablePagination = new TablePagination(getDriver());
        visit();

        final int tableRecordsCount = tablePagination.getTableRecordsCount();

        assertThat(tableRecordsCount)
                .isLessThan(15)
                .isEqualTo(13);
    }

    @Test
    public void shouldCheckIfTableHasExactly13Rows(){
        tablePagination = new TablePagination(getDriver());
        visit();

        final int tableRecordsCount = tablePagination.getTableRecordsCount();

        assertThat(tableRecordsCount)
                .isEqualTo(13);
    }

    @Test
    public void shouldCheckIfLastRecordIsDisplayedInLastPage(){
        tablePagination = new TablePagination(getDriver());
        visit();

        tablePagination.goToLastPageThroughEachPage();
        boolean isLastRecordDisplayed = tablePagination.isRecordNumberDisplayed(13);

        assertThat(isLastRecordDisplayed)
                .as("Check if LAST record of pagination table is visible on the LAST page")
                .isTrue();
    }

    private void visit() {
        getBrowserTools().visit("https://www.seleniumeasy.com/test/table-pagination-demo.html");
    }

}
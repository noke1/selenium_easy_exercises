package helper.pages.input_forms;

import helper.others.waiter.Waiter;
import helper.pages.core.CorePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SimpleFormDemo extends CorePage {

    public SimpleFormDemo(WebDriver driver) {
        super(driver);
    }

    @FindBy (id = "user-message")
    private WebElement inputSingleField;

    @FindBy (css = "#get-input button")
    private WebElement buttonShowMessage;

    @FindBy (id = "display")
    private WebElement spanYourMessage;

    @FindBy (id = "sum1")
    private WebElement inputEnterA;

    @FindBy (id = "sum2")
    private WebElement inputEnterB;

    @FindBy (css = "#gettotal button")
    private WebElement buttonGetTotal;

    @FindBy (id = "displayvalue")
    private WebElement spanTotal;


    public SimpleFormDemo enterMessage(String message) {
        Waiter.waitForElementToAppear(inputSingleField);
        inputSingleField.sendKeys(message);
        return this;
    }

    public SimpleFormDemo clickShowMessage() {
        Waiter.waitForElementToAppear(buttonShowMessage);
        buttonShowMessage.click();
        return this;
    }

    public String getYourMessage() {
        Waiter.waitForElementToAppear(spanYourMessage);
        return spanYourMessage.getText();
    }

    public SimpleFormDemo enterValueA(int value) {
        Waiter.waitForElementToAppear(inputEnterA);
        inputEnterA.sendKeys(String.valueOf(value));
        return this;
    }

    public SimpleFormDemo enterValueB(int value) {
        Waiter.waitForElementToAppear(inputEnterB);
        inputEnterB.sendKeys(String.valueOf(value));
        return this;
    }

    public SimpleFormDemo clickGetTotal() {
        Waiter.waitForElementToBeClickable(buttonGetTotal);
        buttonGetTotal.click();
        return this;
    }

    public int getTotal() {
        Waiter.waitForElementToAppear(spanTotal);
        return Integer.parseInt(spanTotal.getText());
    }


}

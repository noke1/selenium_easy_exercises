package helper.pages.progress_bar_sliders;

import helper.pages.core.CorePage;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class JQueryProgressBar extends CorePage {

    public JQueryProgressBar(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "downloadButton")
    private WebElement buttonStartDownload;

    @FindBy(css = "div.progress-label")
    private WebElement progressBar;


    public JQueryProgressBar startDownload() {
        buttonStartDownload.click();
        return this;
    }

    public String getCompleteMessage() {
        String progress = "";
        final String completeMessage = "Complete!";
        LocalDateTime then = LocalDateTime.now();
        final int timeout_in_seconds = 45;

        do {
            progress = progressBar.getText();
            if(ChronoUnit.SECONDS.between(then,LocalDateTime.now()) >= timeout_in_seconds)
                throw new TimeoutException("Complete message not visible after 3");
        } while (!progress.equals(completeMessage));

        return progress;
    }




}

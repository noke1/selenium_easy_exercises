package helper.pages.input_forms;

import helper.pages.core.CorePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static helper.others.waiter.Waiter.waitForElementToAppear;

public class RadioButtonsDemo extends CorePage {

    public RadioButtonsDemo(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//input[@value='Male' and @name='optradio']")
    private WebElement radioMale;

    @FindBy(xpath = "//input[@value='Female' and @name='optradio']")
    private WebElement radioFemale;

    @FindBy(id = "buttoncheck")
    private WebElement buttonGetCheckedValue;

    @FindBy(css = "p.radiobutton")
    private WebElement paragraphValue;
    //
    @FindBy(xpath = "//button[contains(text(),'Get values')]")
    private WebElement buttonGetValues;

    @FindBy(xpath = "//input[@value='Male' and @name='gender']")
    private WebElement radioGenderMale;

    @FindBy(xpath = "//input[@value='Female' and @name='gender']")
    private WebElement radioGenderFemale;

    @FindBy(xpath = "//input[@value='0 - 5' and @name='ageGroup']")
    private WebElement radioAge0to5;

    @FindBy(xpath = "//input[@value='5 - 15' and @name='ageGroup']")
    private WebElement radioAge5to15;

    @FindBy(xpath = "//input[@value='15 - 50' and @name='ageGroup']")
    private WebElement radioAge15to50;

    @FindBy(css = "p.groupradiobutton")
    private WebElement paragraphSummary;



    public RadioButtonsDemo selectRadioMale() {
        waitForElementToAppear(radioMale);
        radioMale.click();
        return this;
    }

    public RadioButtonsDemo selectRadioFemale() {
        waitForElementToAppear(radioFemale);
        radioFemale.click();
        return this;
    }


    public String getValueAfterSelectRadio() {
        waitForElementToAppear(paragraphValue);
        return paragraphValue.getText();
    }

    public RadioButtonsDemo clickGetCheckedValuesButton() {
        waitForElementToAppear(buttonGetCheckedValue);
        buttonGetCheckedValue.click();
        return this;
    }

    public RadioButtonsDemo selectGenderOf(Gender gender) {
        switch (gender) {
            case MALE:
                waitForElementToAppear(radioGenderMale);
                radioGenderMale.click();
                return this;
            case FEMALE:
                waitForElementToAppear(radioGenderFemale);
                radioGenderFemale.click();
                return this;
            default:
                throw new IllegalArgumentException("Wrong value passed");
        }
    }


    public RadioButtonsDemo selectAgeOf(Age age) {
        switch (age) {
            case ZERO_TO_FIVE:
                waitForElementToAppear(radioAge0to5);
                radioAge0to5.click();
                return this;
            case FIVE_TO_FIFTEEN:
                waitForElementToAppear(radioAge5to15);
                radioAge5to15.click();
                return this;
            case FIFTEEN_TO_FIFTY:
                waitForElementToAppear(radioAge15to50);
                radioAge15to50.click();
                return this;
            default:
                throw new IllegalArgumentException("Wrong value passed");
        }
    }

    public RadioButtonsDemo clickGetValues() {
        waitForElementToAppear(buttonGetValues);
        buttonGetValues.click();
        return this;
    }

    public String getValuesSummary() {
        waitForElementToAppear(paragraphSummary);
        return paragraphSummary.getText();
    }


    public enum Gender {
        FEMALE,
        MALE
    }

    public enum Age {
        ZERO_TO_FIVE,
        FIVE_TO_FIFTEEN,
        FIFTEEN_TO_FIFTY
    }




}

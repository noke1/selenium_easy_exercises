package helper.pages.input_forms;

import helper.others.waiter.Waiter;
import helper.pages.core.CorePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class CheckboxDemo extends CorePage {

    public CheckboxDemo(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "isAgeSelected")
    private WebElement checkboxAge;

    @FindBy(id = "txtAge")
    private WebElement successMessage;

    @FindBy(css = "input.cb1-element")
    private List<WebElement> multipleCheckboxes;

    @FindBy(id = "check1")
    private WebElement buttonCheckAll;

    public CheckboxDemo selectCheckboxAge() {
        Waiter.waitForElementToAppear(checkboxAge);
        checkboxAge.click();
        return this;
    }

    public String getSuccessMessage() {
        Waiter.waitForElementToAppear(successMessage);
        return successMessage.getText();
    }

    public CheckboxDemo clickCheckAll() {
        Waiter.waitForElementToAppear(buttonCheckAll);
        buttonCheckAll.click();
        return this;
    }

    public boolean isCheckboxesSelected() {
        multipleCheckboxes.forEach(Waiter::waitForElementToAppear);
        return multipleCheckboxes.stream()
                .allMatch(WebElement::isSelected);
    }

    public String getCheckAllButtonText() {
        Waiter.waitForElementToAppear(buttonCheckAll);
        return buttonCheckAll.getAttribute("value");
    }

    public CheckboxDemo deselectOneRandomCheckbox() {
        multipleCheckboxes.forEach(Waiter::waitForElementToAppear);
        final int numberOfCheckboxes = multipleCheckboxes.size();
        final int randomNumber = ThreadLocalRandom.current().nextInt(numberOfCheckboxes);
        multipleCheckboxes.get(randomNumber).click();
        return this;
    }
}

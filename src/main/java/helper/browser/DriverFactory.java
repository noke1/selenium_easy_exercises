package helper.browser;

import org.openqa.selenium.WebDriver;

public class DriverFactory {


    private DriverFactory() {
    }

    private static DriverFactory instance = new DriverFactory();

    public static DriverFactory getInstance() {
        return instance;
    }

    ThreadLocal<WebDriver> driver = new ThreadLocal<>();

    public WebDriver getDriver() {
        return driver.get();
    }

    public void setDriver(WebDriver newDriver) {
        driver.set(newDriver);
    }

    public void adjustDriver() {
        driver.get().manage().deleteAllCookies();
        driver.get().manage().window().maximize();
    }

    public void cleanUp() {
//        driver.get().close();
        driver.get().quit();
        driver.remove();
    }

}

package date_pickers;

import core.CoreTest;
import helper.pages.date_pickers.BootstrapDatePickers;
import org.testng.annotations.Test;

import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class BootstrapDatePickersTest extends CoreTest {

    private BootstrapDatePickers bootstrapDatePickers;

    @Test(groups = {"smoke"})
    public void shouldPickDateFromDatePicker() {
        bootstrapDatePickers = new BootstrapDatePickers(getDriver());
        final LocalDate testDate = LocalDate.of(2020, Month.OCTOBER, 30);
        visit();

        bootstrapDatePickers
                .openDatePickerExampleDate()
                .selectDateOf(testDate);
        LocalDate actualDate = bootstrapDatePickers.getExampleDate();

        assertThat(testDate).isEqualTo(actualDate);
    }

    @Test
    public void shouldInsertDateIntoInputField() {
        bootstrapDatePickers = new BootstrapDatePickers(getDriver());
        final LocalDate testDate = LocalDate.of(2020, Month.OCTOBER, 30);
        visit();

        bootstrapDatePickers
                .insertDateOf(testDate);
        LocalDate actualDate = bootstrapDatePickers.getExampleDate();

        assertThat(testDate).isEqualTo(actualDate);
    }

    @Test
    public void shouldPickStartDateAndEndDate() throws InterruptedException {
        bootstrapDatePickers = new BootstrapDatePickers(getDriver());
        final LocalDate startDate = LocalDate.of(2020, Month.SEPTEMBER, 1);
        final LocalDate endDate = LocalDate.of(2020, Month.NOVEMBER, 3);
        final List<LocalDate> datesStartEnd = Arrays.asList(startDate, endDate);
        visit();

        bootstrapDatePickers
                .openDatePickerStartDate()
                .selectDateOf(startDate)
                .openDatePickerEndDate()
                .selectDateOf(endDate);
        List<LocalDate> actualDates = Arrays.asList(
                bootstrapDatePickers.getStartDate(),
                bootstrapDatePickers.getEndDate()
        );

        assertThat(actualDates).isEqualTo(datesStartEnd);
    }

    private void visit() {
        getBrowserTools().visit("https://www.seleniumeasy.com/test/bootstrap-date-picker-demo.html");
    }

}
package helper.pages.table;

import helper.others.pojo.Color;
import helper.others.pojo.ColorOption;
import helper.pages.core.CorePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import java.util.stream.Collectors;

public class TableFilter extends CorePage {

    @FindBy(css = "tbody tr:not([style='display: none;']) h4 span")
    private List<WebElement> availableColors;

    @FindBy(css = "button[data-target='pagado']")
    private WebElement buttonGreen;

    @FindBy(css = "button[data-target='pendiente']")
    private WebElement buttonOrange;

    @FindBy(css = "button[data-target='cancelado']")
    private WebElement buttonRed;

    @FindBy(css = "button[data-target='all']")
    private WebElement buttonAll;

    public TableFilter(WebDriver driver) {
        super(driver);
    }

    public TableFilter filterBy(ColorOption option) {
        switch (option) {
            case GREEN:
                buttonGreen.click();
                break;
            case RED:
                buttonRed.click();
                break;
            case ORANGE:
                buttonOrange.click();
                break;
            case ALL:
                buttonAll.click();
                break;
            default:
                throw new IllegalArgumentException("Filter option is not in range of defined options");
        }
        return this;
    }

    public List<Color> getFilterResults() {
        return mapResultsToColors();
    }

    private List<Color> mapResultsToColors() {
        return availableColors.stream()
                .map(WebElement::getText)
                .map(this::removeBrackets)
                .map(Color::new)
                .collect(Collectors.toList());
    }

    private String removeBrackets(String x) {
        return x.replaceAll("[()]", "");
    }
}


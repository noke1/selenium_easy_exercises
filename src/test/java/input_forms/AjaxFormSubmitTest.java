package input_forms;

import core.CoreTest;
import helper.pages.input_forms.AjaxFormSubmit;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AjaxFormSubmitTest extends CoreTest {

    private AjaxFormSubmit ajaxFormSubmit;

    @Test
    public void checkIfConfirmationAppearsAfterSubmit() {
        ajaxFormSubmit = new AjaxFormSubmit(getDriver());
        visit();

        ajaxFormSubmit
                .typeName("123")
                .typeComment("123")
                .submitForm();
        final String successMessage = ajaxFormSubmit.getSuccessMessage();

        assertThat(successMessage).isEqualTo("Form submited Successfully!");
    }

    private void visit() {
        getBrowserTools().visit("https://www.seleniumeasy.com/test/ajax-form-submit-demo.html");
    }

}

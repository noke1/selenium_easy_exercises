package helper.pages.date_pickers;

import helper.pages.core.CorePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.time.LocalDate;
import java.util.List;

import static helper.others.waiter.Waiter.*;

public class BootstrapDatePickers extends CorePage {

    public BootstrapDatePickers(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "#sandbox-container1 span")
    private WebElement spanOpenDatePicker;

    @FindBy(css = ".datepicker-days .datepicker-switch")
    private WebElement actualDateAndYear;

    @FindBy(css = ".datepicker-days .prev")
    private WebElement goBack;

    @FindBy(css = ".datepicker-days td[class='day']")
    private List<WebElement> daysOfMonth;

    @FindBy(css = "#sandbox-container1 input")
    private WebElement inputDate;

    @FindBy(css = "#datepicker input[placeholder='Start date']")
    private WebElement inputStartDate;

    @FindBy(css = "#datepicker input[placeholder='End date']")
    private WebElement inputEndDate;

    private DatePicker datePicker;
    private final String datePickerLocator = "div.datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-top";

    public BootstrapDatePickers openDatePickerExampleDate() {
        waitForElementToAppear(spanOpenDatePicker);
        spanOpenDatePicker.click();
        return this;
    }

    public BootstrapDatePickers openDatePickerStartDate() {
        waitForElementToAppear(inputStartDate);
        inputStartDate.click();
        return this;
    }

    public BootstrapDatePickers openDatePickerEndDate() {
        waitForElementToAppear(inputEndDate);
        inputEndDate.click();
        return this;
    }

    public BootstrapDatePickers selectDateOf(LocalDate date) {
        initDatePicker(datePickerLocator);
        datePicker.selectDateOf(date);
        return this;
    }

    public LocalDate getExampleDate() {
        return getLocalDateFromField(inputDate);
    }

    public LocalDate getStartDate() {
        return getLocalDateFromField(inputStartDate);
    }

    public LocalDate getEndDate() {
        return getLocalDateFromField(inputEndDate);
    }

    public BootstrapDatePickers insertDateOf(LocalDate chosenDate) {
        waitForElementToAppear(inputDate);
        int chosenDay = chosenDate.getDayOfMonth();
        int chosenMonth = chosenDate.getMonth().getValue();
        int chosenYear = chosenDate.getYear();
        inputDate.sendKeys(chosenDay + "/" + chosenMonth + "/" + chosenYear);
        return this;
    }

    private LocalDate getLocalDateFromField(WebElement input) {
        waitForElementToAppear(input);
        final String[] split = input.getAttribute("value").split("/");
        return LocalDate.of(
                Integer.parseInt(split[2]), //year
                Integer.parseInt(split[1]), //month
                Integer.parseInt(split[0]) //day
        );
    }

    private void initDatePicker(String locator){
        datePicker = new DatePicker(getDriver().findElement(By.cssSelector(locator)));
    }
}

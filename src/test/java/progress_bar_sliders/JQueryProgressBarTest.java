package progress_bar_sliders;

import core.CoreTest;
import helper.pages.progress_bar_sliders.JQueryProgressBar;
import org.assertj.core.api.Assertions;
import org.testng.annotations.Test;

public class JQueryProgressBarTest extends CoreTest {

    private JQueryProgressBar jQueryProgressBar;


    @Test(groups = {"smoke"})
    public void shouldCheckIfCompleteMessageIsDisplayedAfterDownload() {
        jQueryProgressBar = new JQueryProgressBar(getDriver());
        visit();

        jQueryProgressBar.startDownload();
        String completeMessage = jQueryProgressBar.getCompleteMessage();

        Assertions.assertThat(completeMessage).isEqualTo("Complete!");

    }

    private void visit() {
        getBrowserTools().visit("https://www.seleniumeasy.com/test/jquery-download-progress-bar-demo.html");
    }

}
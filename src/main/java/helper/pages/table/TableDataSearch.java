package helper.pages.table;

import helper.others.pojo.Task;
import helper.others.pojo.User;
import helper.others.waiter.Waiter;
import helper.pages.core.CorePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.List;

import static helper.others.waiter.Waiter.*;

public class TableDataSearch extends CorePage {

    public TableDataSearch(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "#task-table tbody tr:not([style]) td") //i want to see only visible rows
    private List<WebElement> visibleTasksCells;

    @FindBy(id = "task-table-filter")
    private WebElement filterField;
//---------------------------------------------------------------------------
    @FindBy(css = "table[class='table'] tbody tr:not([style]) td")
    private List<WebElement> visibleUserCells;

    @FindBy(css = "button.btn.btn-default.btn-xs.btn-filter")
    private WebElement buttonFilter;

    @FindBy(css = "input[placeholder='#']")
    private WebElement inputFilterById;

    @FindBy(css = "input[placeholder='Username']")
    private WebElement inputFilterByUsername;

    @FindBy(css = "input[placeholder='First Name']")
    private WebElement inputFilterByFirstName;

    @FindBy(css = "input[placeholder='Last Name']")
    private WebElement inputFilterByLastName;

    @FindBy(css = "tr.no-result.text-center td")
    private WebElement errorNoResults;


    public TableDataSearch filterFor(String phrase) {
        filterField.sendKeys(phrase);
        return this;
    }

    public List<Task> getTaskSearchResults() {
        return mapTableToTasks();
    }
    private List<Task> mapTableToTasks() {
        List<Task> tasks = new ArrayList<>();
        for (int i = 0; i < visibleTasksCells.size()-3; i+=4) {
            tasks.add(new Task(
                Integer.parseInt(visibleTasksCells.get(i).getAttribute("outerText")),
                visibleTasksCells.get(i+1).getAttribute("outerText"),
                visibleTasksCells.get(i+2).getAttribute("outerText"),
                visibleTasksCells.get(i+3).getAttribute("outerText")
            ));
        }
        return tasks;
    }

//---------------------------------------------------

    public TableDataSearch activateFilters() {
        buttonFilter.click();
        return this;
    }
    public TableDataSearch filterByIdOf(int id) {
        inputFilterById.sendKeys(String.valueOf(id));
        return this;
    }

    public TableDataSearch filterByUserNameOf(String userName) {
        inputFilterByUsername.sendKeys(userName);
        return this;
    }

    public TableDataSearch filterByFirstNameOf(String firstName) {
        inputFilterByFirstName.sendKeys(firstName);
        return this;
    }

    public TableDataSearch filterByLastNameOf(String lastName) {
        inputFilterByLastName.sendKeys(lastName);
        return this;
    }

    public boolean isErrorNoResultsDisplayed() {
        waitForElementToAppear(errorNoResults);
        return errorNoResults.isDisplayed();
    }

    public List<User> getUserSearchResults() {
        return mapTableToUsers();
    }

    private List<User> mapTableToUsers() {
        List<User> users = new ArrayList<>();
        for (int i = 0; i < visibleUserCells.size()-3; i+=4) {
            users.add(new User(
                Integer.parseInt(visibleUserCells.get(i).getAttribute("outerText")),
                visibleUserCells.get(i+1).getAttribute("outerText"),
                visibleUserCells.get(i+2).getAttribute("outerText"),
                visibleUserCells.get(i+3).getAttribute("outerText")
            ));
        }
        return users;
    }



}

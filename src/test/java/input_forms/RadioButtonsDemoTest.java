package input_forms;

import core.CoreTest;
import helper.pages.input_forms.RadioButtonsDemo;
import org.testng.annotations.Test;

import static helper.pages.input_forms.RadioButtonsDemo.Age.FIFTEEN_TO_FIFTY;
import static helper.pages.input_forms.RadioButtonsDemo.Gender.MALE;
import static org.assertj.core.api.Assertions.assertThat;

public class RadioButtonsDemoTest extends CoreTest {

    private RadioButtonsDemo radioButtonsDemo;

    @Test(groups = {"smoke"})
    public void checkIfMaleValueIsVisibleAfterClickRadioMale() {
        radioButtonsDemo = new RadioButtonsDemo(getDriver());
        visit();

        radioButtonsDemo
                .selectRadioMale()
                .clickGetCheckedValuesButton();
        final String valueMale = radioButtonsDemo.getValueAfterSelectRadio();

        assertThat(valueMale).isEqualTo("Radio button \'Male\' is checked");
    }

    @Test
    public void checkIfFemaleValueIsVisibleAfterClickRadioFemale() {
        radioButtonsDemo = new RadioButtonsDemo(getDriver());
        visit();

        radioButtonsDemo
                .selectRadioFemale()
                .clickGetCheckedValuesButton();
        final String valueFemale = radioButtonsDemo.getValueAfterSelectRadio();

        assertThat(valueFemale).isEqualTo("Radio button \'Female\' is checked");
    }

    @Test
    public void checkIfCorrectSummaryAppearsForGenderMaleAndAge15to50() {
        radioButtonsDemo = new RadioButtonsDemo(getDriver());
        visit();

        radioButtonsDemo
                .selectGenderOf(MALE)
                .selectAgeOf(FIFTEEN_TO_FIFTY)
                .clickGetValues();
        final String valuesSummary = radioButtonsDemo.getValuesSummary();

        assertThat(valuesSummary).isEqualToNormalizingNewlines("Sex : Male\nAge group: 15 - 50");
    }

    private void visit() {
        getBrowserTools().visit("https://www.seleniumeasy.com/test/basic-radiobutton-demo.html");
    }

}

package core;

import helper.browser.BrowserFactory;
import helper.browser.DriverFactory;
import helper.others.browser_tools.BrowserTools;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

public abstract class CoreTest {

    private WebDriver driver = null;
    private BrowserTools browserTools;

    @BeforeClass
    @Parameters({"browserName"})
    public void beforeClass(@Optional("FIREFOX") String browserName) {
        driver = new BrowserFactory().createInstance(browserName);
        DriverFactory.getInstance().setDriver(driver);
        driver = DriverFactory.getInstance().getDriver();
        DriverFactory.getInstance().adjustDriver();

        browserTools = new BrowserTools(driver);
    }

    @AfterClass
    public void afterClass() {
        DriverFactory.getInstance().cleanUp();
    }

    public WebDriver getDriver() {
        return driver;
    }

    public BrowserTools getBrowserTools() {
        return browserTools;
    }
}

package helper.others.waiter;

import helper.others.reader.ConfigReader;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Waiter {

    private static WebDriverWait wait;
    private WebDriver driver;
    private static final int TIMEOUT = Integer.parseInt(new ConfigReader().getValueOf("timeout"));
    private static final int POLLING = Integer.parseInt(new ConfigReader().getValueOf("polling"));

    public Waiter(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver,TIMEOUT, POLLING);
    }


    public static void waitForElementToAppear(WebElement element) {
        wait
                .ignoring(StaleElementReferenceException.class)
                .until(ExpectedConditions.refreshed(ExpectedConditions.visibilityOf(element)));
    }

    /**
     * This method is for checking if List<WebElement> appeared. The only expected condition for list is
     * presenceOfAllElementsLocatedBy which accepts By instead of WebElement.
     * @param selector
     */
    public static void waitForAllElementsToAppear(By selector) {
        wait
                .ignoring(StaleElementReferenceException.class)
                .until(ExpectedConditions.refreshed(ExpectedConditions.visibilityOfAllElementsLocatedBy(selector)));
    }

    public static void waitForElementToBeClickable(WebElement element) {
        wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(element)));
    }


    /**
     * 1. Enter to a page - web elements are initialized, 2. click button located by @findBy, 3. refresh page,
     * 4. Click the same button again, 5. StaleElementReference Exception!!! This is why this method was created.
     * @param locator
     */
    public static void waitForStaleElementToAppear(WebElement locator) {
        wait.ignoring(StaleElementReferenceException.class)
                .until(ExpectedConditions.refreshed((ExpectedConditions.elementToBeClickable(locator))));
    }

}


package table;

import core.CoreTest;
import helper.others.pojo.Task;
import helper.others.pojo.User;
import helper.pages.table.TableDataSearch;
import org.testng.annotations.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class TableDataSearchTest extends CoreTest {

    private TableDataSearch tableDataSearch;

    @Test(groups = {"smoke"})
    public void shouldFilterTasksByStatus(){
        tableDataSearch = new TableDataSearch(getDriver());
        final String searchPhrase = "in progress";
        visit();

        tableDataSearch.filterFor(searchPhrase);
        final List<Task> searchResults = tableDataSearch.getTaskSearchResults();

        assertThat(searchResults)
                .map(Task::getStatus)
                .allMatch(status -> status.contains(searchPhrase));
    }

    @Test
    public void shouldFilterTasksByAssignee(){
        tableDataSearch = new TableDataSearch(getDriver());
        final String searchPhrase = "John Smith";
        visit();

        tableDataSearch.filterFor(searchPhrase);
        final List<Task> searchResults = tableDataSearch.getTaskSearchResults();

        assertThat(searchResults)
                .map(Task::getAssignee)
                .allMatch(x -> x.contains(searchPhrase));
    }

    @Test
    public void shouldFilterTasksByTaskName(){
        tableDataSearch = new TableDataSearch(getDriver());
        final String searchPhrase = "Wireframes";
        visit();

        tableDataSearch.filterFor(searchPhrase);
        final List<Task> searchResults = tableDataSearch.getTaskSearchResults();

        assertThat(searchResults)
                .map(Task::getTaskName)
                .allMatch(x -> x.contains(searchPhrase));
    }

    @Test
    public void shouldFilterTasksById(){
        tableDataSearch = new TableDataSearch(getDriver());
        final String searchPhrase = "1";
        visit();

        tableDataSearch.filterFor(searchPhrase);
        final List<Task> searchResults = tableDataSearch.getTaskSearchResults();

        assertThat(searchResults)
                .map(Task::getId)
                .allMatch(x -> x == Integer.parseInt(searchPhrase));
    }

    @Test
    public void shouldFilterTasksPartialText(){
        tableDataSearch = new TableDataSearch(getDriver());
        final String searchPhrase = "a";
        visit();

        tableDataSearch.filterFor(searchPhrase);
        final List<Task> searchResults = tableDataSearch.getTaskSearchResults();

        assertThat(searchResults)
                .filteredOn(task -> task.getStatus().contains(searchPhrase) ||
                        task.getTaskName().contains(searchPhrase) ||
                        task.getAssignee().contains(searchPhrase)
                ).allMatch(x -> true);
    }
    //-----------------------------------------------------------------------------------------

    @Test
    public void shouldFilterUsersById(){
        tableDataSearch = new TableDataSearch(getDriver());
        final int searchPhrase = 1;
        visit();

        tableDataSearch
                .activateFilters()
                .filterByIdOf(1);
        final List<User> userSearchResults = tableDataSearch.getUserSearchResults();

        assertThat(userSearchResults)
                .map(User::getId)
                .allMatch(x -> x == searchPhrase);
    }

    @Test
    public void shouldFilterUsersByUserName(){
        tableDataSearch = new TableDataSearch(getDriver());
        final String searchPhrase = "larry";
        visit();

        tableDataSearch
                .activateFilters()
                .filterByUserNameOf(searchPhrase);
        final List<User> userSearchResults = tableDataSearch.getUserSearchResults();

        assertThat(userSearchResults)
                .map(User::getUserName)
                .allMatch(x -> x.contains(searchPhrase));
    }

    @Test
    public void shouldFilterUsersByFirstName(){
        tableDataSearch = new TableDataSearch(getDriver());
        final String searchPhrase = "Daniel";
        visit();

        tableDataSearch
                .activateFilters()
                .filterByFirstNameOf(searchPhrase);
        final List<User> userSearchResults = tableDataSearch.getUserSearchResults();

        assertThat(userSearchResults)
                .map(User::getFirstName)
                .allMatch(x -> x.contains(searchPhrase));
    }

    @Test
    public void shouldFilterUsersByLastName(){
        tableDataSearch = new TableDataSearch(getDriver());
        final String searchPhrase = "Swarroon";
        visit();

        tableDataSearch
                .activateFilters()
                .filterByLastNameOf(searchPhrase);
        final List<User> userSearchResults = tableDataSearch.getUserSearchResults();

        assertThat(userSearchResults)
                .map(User::getLastName)
                .allMatch(x -> x.contains(searchPhrase));
    }

    @Test
    public void shouldDisplayErrorWhenNonExistingSearchPhraseProvided(){
        tableDataSearch = new TableDataSearch(getDriver());
        final String nonExistingLastName = "XXXXXX123123";
        visit();

        tableDataSearch
                .activateFilters()
                .filterByLastNameOf(nonExistingLastName);
        final boolean errorNoResultsDisplayed = tableDataSearch.isErrorNoResultsDisplayed();

        assertThat(errorNoResultsDisplayed)
                .as("Check if message \'No results found\' is visible after passing non exiting data into filter field")
                .isTrue();
    }

    private void visit() {
        getBrowserTools().visit("https://www.seleniumeasy.com/test/table-search-filter-demo.html");
    }

}
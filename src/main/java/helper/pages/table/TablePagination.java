package helper.pages.table;

import helper.pages.core.CorePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class TablePagination extends CorePage {

    public TablePagination(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "#myTable tr > td:nth-child(1)")
    private List<WebElement> rowNumbers;

    @FindBy(css = "a.page_link")
    private List<WebElement> pages;

    public int getTableRecordsCount() {
        return rowNumbers.size();
    }

    public TablePagination goToLastPageThroughEachPage() {
        pages.forEach(WebElement::click);
        return this;
    }


    public boolean isRecordNumberDisplayed(int number) {
        return rowNumbers.stream()
                .filter(x -> (Integer.parseInt(x.getAttribute("innerText"))) == number)
                .findFirst()
                .filter(WebElement::isDisplayed)
                .isPresent();
    }

}

package input_forms;

import core.CoreTest;
import helper.pages.input_forms.SimpleFormDemo;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class SimpleFormDemoTest extends CoreTest {

    private SimpleFormDemo simpleFormDemo;

    @Test
    public void checkIfSingleInputMessageIsSameAsEntered() {
        simpleFormDemo = new SimpleFormDemo(getDriver());
        visit();
        final String searchPhrase = "TEST";

        simpleFormDemo
                .enterMessage(searchPhrase)
                .clickShowMessage();
        final String yourMessage = simpleFormDemo.getYourMessage();

        assertThat(yourMessage)
                .as("Message after displaying should be same as entered")
                .isEqualTo(searchPhrase);
    }


    @Test(groups = {"smoke"})
    public void checkIfEnteredValuesSumIsCorrect() {
        simpleFormDemo = new SimpleFormDemo(getDriver());
        visit();
        final int valueA = 1;
        final int valueB = 2;

        simpleFormDemo
                .enterValueA(valueA)
                .enterValueB(valueB)
                .clickGetTotal();
        final int calculatedTotal = simpleFormDemo.getTotal();

        assertThat(calculatedTotal)
                .as("After inserting to values total should be equal to its sum")
                .isEqualTo(valueA + valueB);
    }

    private void visit() {
        getBrowserTools().visit("https://www.seleniumeasy.com/test/basic-first-form-demo.html");
    }
}

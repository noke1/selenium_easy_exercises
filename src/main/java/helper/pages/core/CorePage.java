package helper.pages.core;

import helper.browser.DriverFactory;
import helper.others.waiter.Waiter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public abstract class CorePage {

    private final WebDriver driver;
    private final Waiter waiter;

    public CorePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver,this);

        this.waiter = new Waiter(driver);
    }

    public WebDriver getDriver() {
        return driver;
    }
}

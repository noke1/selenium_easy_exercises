package helper.pages.input_forms;

import helper.pages.core.CorePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static helper.others.waiter.Waiter.waitForElementToAppear;

public class AjaxFormSubmit extends CorePage {

    public AjaxFormSubmit(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "title")
    private WebElement inputName;

    @FindBy(id = "description")
    private WebElement inputComment;

    @FindBy(id = "btn-submit")
    private WebElement inputSubmit;

    @FindBy(xpath = "//div[contains(text(),'Successfully!')]")
    private WebElement successMessage;


    public AjaxFormSubmit typeName(String text) {
        waitForElementToAppear(inputName);
        inputName.sendKeys(text);
        return this;
    }

    public AjaxFormSubmit typeComment(String text) {
        waitForElementToAppear(inputComment);
        inputComment.sendKeys(text);
        return this;
    }

    public AjaxFormSubmit submitForm() {
        waitForElementToAppear(inputSubmit);
        inputSubmit.click();
        return this;
    }

    public String getSuccessMessage() {
        waitForElementToAppear(successMessage);
        return successMessage.getText();
    }
}

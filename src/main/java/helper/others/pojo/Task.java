package helper.others.pojo;

public class Task {

    private int id;
    private String taskName;
    private String assignee;
    private String status;

    public Task(int id, String taskName, String assignee, String status) {
        this.id = id;
        this.taskName = taskName;
        this.assignee = assignee;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getAssignee() {
        return assignee;
    }

    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

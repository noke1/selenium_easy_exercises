package helper.pages.progress_bar_sliders;

import helper.others.waiter.Waiter;
import helper.pages.core.CorePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static helper.others.waiter.Waiter.*;

public class BootstrapProgressBar extends CorePage {

    public BootstrapProgressBar(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//div[text()='100%']")
    private WebElement hundredPercentMessage;

    @FindBy(id = "cricle-btn")
    private WebElement buttonStartDownload;


    public BootstrapProgressBar startDownload() {
        buttonStartDownload.click();
        return this;
    }

    public boolean waitForDownloadToFinish() {
        waitForElementToAppear(hundredPercentMessage);
        return hundredPercentMessage.isDisplayed();
    }
}

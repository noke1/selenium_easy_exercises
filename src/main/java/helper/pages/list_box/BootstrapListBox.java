package helper.pages.list_box;

import helper.pages.core.CorePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import java.util.stream.Collectors;

public class BootstrapListBox extends CorePage {

    @FindBy(css = ".dual-list.list-left.col-md-5 li")
    private List<WebElement> leftList;

    @FindBy(css = ".dual-list.list-right.col-md-5 li")
    private List<WebElement> rightList;

    @FindBy(css = "span.glyphicon.glyphicon-chevron-right")
    private WebElement spanMoveToRightList;

    @FindBy(css = ".dual-list.list-left.col-md-5 #listhead a")
    private WebElement buttonSelectAllItemsFromLeftList;

    @FindBy(css = ".dual-list.list-left.col-md-5 input[name='SearchDualList']")
    private WebElement leftSearch;

    public BootstrapListBox(WebDriver driver) {
        super(driver);
    }

    public BootstrapListBox selectFromLeftList(List<String> phrases) {
        for (WebElement webElement : leftList) {
            selectFromLeftList(webElement.getText());
        }
        return this;
    }

    public BootstrapListBox selectFromLeftList(String phrase) {
        for (WebElement webElement : leftList) {
            if(webElement.getText().equals(phrase)) webElement.click();
        }
        return this;
    }

    public BootstrapListBox moveSelectionToRightList() {
        spanMoveToRightList.click();
        return this;
    }

    public List<String> getRightListContent() {
        return rightList.stream()
                .map(WebElement::getText)
                .collect(Collectors.toList());
    }

    public List<String> getLeftListContent() {
        return leftList.stream()
                .map(WebElement::getText)
                .collect(Collectors.toList());
    }

    public BootstrapListBox selectAllItemsFromLeftList() {
        buttonSelectAllItemsFromLeftList.click();
        return this;
    }

    public BootstrapListBox searchInLeftListFor(String searchPhrase) {
        leftSearch.sendKeys(searchPhrase);

        return this;
    }
}

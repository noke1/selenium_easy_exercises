package helper.pages.progress_bar_sliders;

import helper.pages.core.CorePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class RangeSliders extends CorePage {
    public RangeSliders(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "range")
    private WebElement actualRange;

    @FindBy(css = "div[class='range'] input")
    private WebElement slider;


    public RangeSliders adjustSliderTo(int value) {
        int min = Integer.parseInt(slider.getAttribute("min"));
        int max = Integer.parseInt(slider.getAttribute("max"));
        double width = slider.getSize().getWidth();
        if(value > max || value < min) throw new IllegalArgumentException(String.format("Given value is not in range of %d - %d", min,max));

        Actions actions = new Actions(getDriver());
        actions.moveToElement(slider)
                .clickAndHold()
                .moveByOffset((int) -(width/2),0)
                .moveByOffset((int) ((width/max)*value), 0)
                .release()
                .build()
                .perform();
        return this;
    }

    public int getSliderActualValue() {
        return Integer.parseInt(actualRange.getText());
    }
}

package helper.pages.table;

import helper.others.pojo.Pagination;
import helper.others.pojo.Person;
import helper.pages.core.CorePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class TableSortAndSearch extends CorePage {

    public TableSortAndSearch(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "select[name='example_length']")
    private WebElement selectPaginationOptions;

    @FindBy(css = "tbody tr")
    private List<WebElement> tableRows;


    public TableSortAndSearch paginateTable(Pagination option) {
        final Select select = new Select(selectPaginationOptions);
        select.selectByValue(String.valueOf(option.getNumber()));
        return this;
    }

    public List<WebElement> getTableRecords() {
        return tableRows;
    }
}

package helper.others.pojo;

public enum Pagination {
    TEN(10),TWENTY_FIVE(25),FIFTY(50),HUNDRED(100);

    private int number;

    Pagination(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }
}

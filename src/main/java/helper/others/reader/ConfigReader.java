package helper.others.reader;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ConfigReader {

    public String getValueOf(String property) {
        FileInputStream fileInputStream = null;
        String valueForProperty = "";
        try {
             fileInputStream = new FileInputStream("src/main/resources/app.properties");
            Properties properties = new Properties();
            properties.load(fileInputStream);
            valueForProperty = properties.getProperty(property);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return valueForProperty;
    }

}

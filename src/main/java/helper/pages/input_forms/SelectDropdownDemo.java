package helper.pages.input_forms;

import helper.pages.core.CorePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import static helper.others.waiter.Waiter.waitForElementToAppear;

public class SelectDropdownDemo extends CorePage {
    public SelectDropdownDemo(WebDriver driver) {
        super(driver);
    }


    @FindBy(id = "select-demo")
    private WebElement selectDaysOfWeek;

    @FindBy(css = "p.selected-value")
    private WebElement paragraphSummary;

    @FindBy(id = "multi-select")
    private WebElement multiSelectStates;

    @FindBy(id = "printMe")
    private WebElement buttonFirstSelected;

    @FindBy(id = "printAll")
    private WebElement buttonGetAllSelected;

    @FindBy(css = ".getall-selected")
    private WebElement paragrapthSummaryForSelectedstates;

    public SelectDropdownDemo chooseDay(String day) {
        waitForElementToAppear(selectDaysOfWeek);
        final Select days = new Select(selectDaysOfWeek);
        days.selectByValue(day);
        return this;
    }

    public String getSummaryAfterDayChosen() {
        waitForElementToAppear(paragraphSummary);
        return paragraphSummary.getText();
    }

    public SelectDropdownDemo chooseState(String state) {
        waitForElementToAppear(multiSelectStates);
        final Select select = new Select(multiSelectStates);
        select.selectByValue(state);
        return this;
    }

    public SelectDropdownDemo clickButtonFirstSelected() {
        waitForElementToAppear(buttonFirstSelected);
        buttonFirstSelected.click();
        return this;
    }

    public SelectDropdownDemo clickButtonGetAllSelected() {
        waitForElementToAppear(buttonGetAllSelected);
        buttonGetAllSelected.click();
        return this;
    }

    public String getSummaryForSelectedStates() {
        waitForElementToAppear(paragrapthSummaryForSelectedstates);
        return paragrapthSummaryForSelectedstates.getText();
    }
}

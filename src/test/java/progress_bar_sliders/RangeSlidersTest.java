package progress_bar_sliders;

import core.CoreTest;
import helper.pages.progress_bar_sliders.RangeSliders;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class RangeSlidersTest extends CoreTest {

    private RangeSliders rangeSliders;

    @Test(dataProvider = "getSliderValues")
    public void shouldMoveSliderToSpecifiedValue(int value) {
        rangeSliders = new RangeSliders(getDriver());
        visit();

        rangeSliders.adjustSliderTo(value);
        final int actualSliderValue = rangeSliders.getSliderActualValue();

        assertThat(actualSliderValue).isEqualTo(value);
    }

    @DataProvider
    public Object[][] getSliderValues() {
        return new Object[][]{
                {1},{100},{50},{33}
        };
    }

    private void visit() {
        getBrowserTools().visit("https://www.seleniumeasy.com/test/drag-drop-range-sliders-demo.html");
    }

}
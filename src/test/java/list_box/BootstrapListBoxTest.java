package list_box;

import core.CoreTest;
import helper.pages.list_box.BootstrapListBox;
import org.assertj.core.api.Assertions;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

public class BootstrapListBoxTest extends CoreTest {

    private BootstrapListBox bootstrapListBox;

    @Test
    public void shouldCheckIfAllSelectedByHandAreMovedToRight() {
        List<String> select = Arrays.asList("bootstrap-duallist", "Morbi leo risus", "Dapibus ac facilisis in", "Porta ac consectetur ac", "Vestibulum at eros");
        bootstrapListBox = new BootstrapListBox(getDriver());
        visit();

        bootstrapListBox
                .selectFromLeftList(select)
                .moveSelectionToRightList();
        List<String> rightListContent = bootstrapListBox.getRightListContent();

        Assertions.assertThat(rightListContent).containsAll(select);
    }

    @Test
    public void shouldCheckIfAllSelectedByButtonAreMovedToRight() {
        bootstrapListBox = new BootstrapListBox(getDriver());
        visit();
        List<String> leftListContent = bootstrapListBox.getLeftListContent();

        bootstrapListBox
                .selectAllItemsFromLeftList()
                .moveSelectionToRightList();
        List<String> rightListContent = bootstrapListBox.getRightListContent();

        Assertions.assertThat(rightListContent).containsAll(leftListContent);
    }

    @Test
    public void shouldCheckIfSearchedItemIsMovedToRight() {
        final String morbi = "Morbi leo risus";
        bootstrapListBox = new BootstrapListBox(getDriver());
        visit();

        bootstrapListBox
                .searchInLeftListFor(morbi)
                .selectFromLeftList(morbi)
                .moveSelectionToRightList();
        long morbiOccurrences = bootstrapListBox.getRightListContent().stream()
                .filter(x -> x.equals(morbi))
                .count();

        Assertions.assertThat(morbiOccurrences).isEqualTo(2);
    }

    private void visit() {
        getBrowserTools().visit("https://www.seleniumeasy.com/test/bootstrap-dual-list-box-demo.html");
    }

}
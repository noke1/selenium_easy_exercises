package helper.others.browser_tools;

import org.openqa.selenium.WebDriver;

public class BrowserTools {

    private final WebDriver driver;

    public BrowserTools(WebDriver driver) {
        this.driver = driver;
    }

    public void visit(String url){
        driver.get(url);
    }

}
